using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AspExample.Models.Contexts;
using AspExample.Models.Repositories;
using AspExample.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace AspExample.Models.UnitOfWorks
{
    public class UnitOfWork
    {
        private WebUserDbContext Context;

        private Dictionary<Type, object> Repositories;

        public UnitOfWork(WebUserDbContext context)
        {
            Context = context;
            this.Repositories = new Dictionary<Type, object>();
        }

        private IRepository<T> GetRepository<T>() where T : class
        {
            if (Repositories.ContainsKey(typeof(T)))
            {
                return (IRepository<T>)Repositories[typeof(T)];
            }
            IRepository<T> repository = new GenericRepository<T>(Context);
            Repositories.Add(typeof(T), repository);
            return repository;
        }

        private void CommitChanges()
        {
            Context.SaveChanges();
        }

        #region Generic methods
        public EntityEntry<T> Create<T>(T entity) where T : class
        {
            IRepository<T> repository = GetRepository<T>();
            EntityEntry<T> result = repository.Create(entity);
            CommitChanges();
            return result;
        }

        public T Read<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            IRepository<T> repository = GetRepository<T>();
            T result = repository.Read(predicate);
            CommitChanges();
            return result;
        }

        public IQueryable<T> ReadAll<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            IRepository<T> repository = GetRepository<T>();
            IQueryable<T> result = repository.ReadAll(predicate);
            CommitChanges();
            return result;
        }

        public IQueryable<T> ReadAll<T>() where T : class
        {
            IRepository<T> repository = GetRepository<T>();
            IQueryable<T> result = repository.ReadAll();
            CommitChanges();
            return result;
        }

        public void Update<T>(T entity) where T : class
        {
            IRepository<T> repository = GetRepository<T>();
            CommitChanges();
            repository.Update(entity);
        }

        public void Update<T>(IEnumerable<T> entities) where T : class
        {
            IRepository<T> repository = GetRepository<T>();
            CommitChanges();
            repository.Update(entities);
        }

        public EntityEntry<T> Delete<T>(T entity) where T : class
        {
            IRepository<T> repository = GetRepository<T>();
            EntityEntry<T> result = repository.Delete(entity);
            CommitChanges();
            return result;
        }
        #endregion
    }
}