
using Microsoft.EntityFrameworkCore;
using AspExample.Models.Pocos;

namespace AspExample.Models.Contexts
{
    public class WebUserDbContext : DbContext
    {
        public DbSet<Happening> Happenings { get; set; }
        public DbSet<Rating> Ratings { get; set; }

        public WebUserDbContext(DbContextOptions<WebUserDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Rating>().HasData(
                new Rating { Id = 1, Text = "Unspecified", },
                new Rating { Id = 2, Text = "1/5", },
                new Rating { Id = 3, Text = "1.5/5", },
                new Rating { Id = 4, Text = "2/5", },
                new Rating { Id = 5, Text = "2.5/5", },
                new Rating { Id = 6, Text = "3/5", },
                new Rating { Id = 7, Text = "3.5/5", },
                new Rating { Id = 8, Text = "4/5", },
                new Rating { Id = 9, Text = "4.5/5", },
                new Rating { Id = 10, Text = "5/5", }
            );
        }
    }

}
