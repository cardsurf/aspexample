
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AspExample.Models.Pocos;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace AspExample.Models.Repositories
{
    public interface IRepository<T> where T : class
    {
        EntityEntry<T> Create(T entity);
        T Read(Expression<Func<T, bool>> predicate);
        IQueryable<T> ReadAll(Expression<Func<T, bool>> predicate);
        IQueryable<T> ReadAll();
        void Update(T entity);
        void Update(IEnumerable<T> entities);
        EntityEntry<T> Delete(T entity);
    }
}