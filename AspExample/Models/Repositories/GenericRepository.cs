
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AspExample.Models.Pocos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace AspExample.Models.Repositories
{
    public class GenericRepository<T> : IRepository<T> where T : class
    {
        private DbContext Context;
        
        public GenericRepository(DbContext context)
        {
            this.Context = context;
        }

        public EntityEntry<T> Create(T entity)
        {
            DbSet<T> entities = this.Context.Set<T>();
            EntityEntry<T> result = entities.Add(entity);
            return result;
        }

        public T Read(Expression<Func<T, bool>> predicate)
        {
            T result = this.ReadAll(predicate).First();
            return result;
        }

        public IQueryable<T> ReadAll(Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> result = this.Context.Set<T>().Where(predicate);
            return result;
        }

        public IQueryable<T> ReadAll()
        {
            IQueryable<T> result = this.Context.Set<T>();
            return result;
        }

        public void Update(T entity)
        {
            DbSet<T> entities = this.Context.Set<T>();
            entities.Attach(entity);
            this.Context.Entry(entity).State = EntityState.Modified;
        }

        public void Update(IEnumerable<T> entities)
        {
            DbSet<T> updatedEntities = this.Context.Set<T>();
            foreach (T entity in entities)
            {
                updatedEntities.Attach(entity);
                this.Context.Entry(entity).State = EntityState.Modified;
            }
        }

        public EntityEntry<T> Delete(T entity)
        {
            DbSet<T> entities = this.Context.Set<T>();
            entities.Attach(entity);
            EntityEntry<T> result = entities.Remove(entity);
            return result;
        }
    }
}
