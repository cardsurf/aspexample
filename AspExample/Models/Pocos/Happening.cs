
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AspExample.Models.Pocos
{
    [Table("Happening")]
    public class Happening
    {
        public UInt64 Id { get; set; }
        public String Name { get; set; }
        public DateTime RegistrationTime { get; set; }

        [ForeignKey("Rating")]
        public UInt64 RatingId { get; set; }
        public Rating Rating { get; set; }
    }
}

