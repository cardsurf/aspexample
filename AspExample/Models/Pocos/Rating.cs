
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AspExample.Models.Pocos
{
    [Table("Rating")]
    public class Rating
    {
        public UInt64 Id { get; set; }
        public String Text { get; set; }
    }
}
