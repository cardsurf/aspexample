﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspExample.Models.Contexts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using AspExample.Models.Repositories;
using AspExample.Models.UnitOfWorks;
using Microsoft.Extensions.Hosting;

namespace AspExample
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(1200);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            string connection = Configuration["ConnectionStrings:SqliteConnectionString"];
            services.AddDbContext<WebUserDbContext>(options => options.UseSqlite(connection));

            services.AddRazorPages();

            services.AddScoped<DbContext, WebUserDbContext>();
            services.AddScoped<UnitOfWork, UnitOfWork>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseStaticFiles();
            app.UseSession();
            app.UseRouting();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}");
                endpoints.MapControllerRoute(
                    name: "NotFound",
                    pattern: "{*url}",
                    defaults: new { controller = "Home", action = "PageNotFound" }
                );
            });
        }
    }
}
