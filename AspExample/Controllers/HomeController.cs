﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspExample.Models.Pocos;
using AspExample.Models.Repositories;
using AspExample.Models.UnitOfWorks;
using AspExample.ViewModels;
using AspExample.ViewModels.Partial;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;

namespace AspExample.Controllers
{
    public class HomeController : Controller
    {
        private const Int32 TableRowsPerPage = 3;
        private const String PageNumberSessionKey = "PageNumber";
        private const Int32 PageNumberDefaultValue = 1;

        private UnitOfWork UnitOfWork;

        public HomeController(UnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("GetHappenings")]
        public IActionResult GetHappenings()
        {
            try
            {
                Int32 maxPage = GetHappeningsMaxPage();
                Int32 page = GetHappeningsPage(maxPage);
                GetHappeningsViewModel model = new GetHappeningsViewModel();
                model.Table = GetHappeningTableViewModels(page, TableRowsPerPage);
                model.Pagination = GetPaginationViewModel(page, maxPage);
                return View("~/Views/Home/GetHappenings.cshtml", model);
            }
            catch (Exception e)
            {
                ViewData["Error"] = "Error while return GetHappenings view: " + e;
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        private Int32 GetHappeningsMaxPage()
        {
            Int32 numberRows = UnitOfWork.ReadAll<Happening>().Count();
            Int32 maxPage = (numberRows % TableRowsPerPage == 0) ?
                            (numberRows / TableRowsPerPage) :
                            (numberRows / TableRowsPerPage) + 1;
            return maxPage;
        }

        private Int32 GetHappeningsPage(Int32 maxPage)
        {
            Int32 page = ReadCreateState(PageNumberSessionKey, PageNumberDefaultValue);
            if(page < 1)
            {
                return 1;
            }
            if(page > maxPage)
            {
                return maxPage;
            }
            return page;
        }   

        private Int32 ReadCreateState(String key, Int32 defaultValue)
        {
            if (string.IsNullOrEmpty(HttpContext.Session.GetString(key)))
            {
                UpdateState(key, defaultValue);
            }
            Int32 result = (Int32) HttpContext.Session.GetInt32(key);
            return result;
        }

        private void UpdateState(String key, Int32 value)
        {
            HttpContext.Session.SetInt32(key, value);
        }
        
        private IEnumerable<GetHappeningsTableViewModel> GetHappeningTableViewModels(
                                                         Int32 page, Int32 count)
        {
            Int32 firstRow = (page - 1) * TableRowsPerPage;
            IEnumerable<GetHappeningsTableViewModel> result =
                        UnitOfWork.ReadAll<Happening>().Skip(firstRow).Take(count).
                        Select(happening => new GetHappeningsTableViewModel()
                        {
                            Id = happening.Id,
                            Name = happening.Name,
                            RatingName = happening.Rating.Text,
                        });
            return result;
        }

        private PaginationTextBoxViewModel GetPaginationViewModel(Int32 page, Int32 maxPage)
        {
            PaginationTextBoxViewModel pagination = new PaginationTextBoxViewModel();
            pagination.CurrentPage = page;
            pagination.MaxPage = maxPage;
            pagination.Controller = "Home";
            pagination.ValueChanged = "OnPaginationUpdated";
            return pagination;
        }

        [HttpGet("OnPaginationUpdated")]
        public IActionResult OnPaginationUpdated(Int32 page)
        {
            try
            {
                UpdateState(PageNumberSessionKey, page);
                return RedirectToAction("GetHappenings");
            }
            catch (Exception e)
            {
                ViewData["Error"] = "Error while updating pagination: " + e;
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        [HttpGet("CreateHappening")]
        public IActionResult CreateHappening()
        {
            try
            {
                CreateUpdateHappeningViewModel model = this.GetEmptyHappeningViewModel();
                String referer = ControllerContext.HttpContext.Request.Headers["Referer"];
                model.PreviousPageAction = referer.EndsWith("GetHappenings") ? 
                                          "GetHappenings" : String.Empty;
                return View("~/Views/Home/CreateUpdateHappening.cshtml", model);
            }
            catch (Exception e)
            {
                ViewData["Error"] = "Error while returning CreateHappening view: " + e;
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        private CreateUpdateHappeningViewModel GetEmptyHappeningViewModel()
        {
            CreateUpdateHappeningViewModel model = new CreateUpdateHappeningViewModel();
            model.Ratings = UnitOfWork.ReadAll<Rating>().
                                       Select(rating => new SelectListItem()
                                       {
                                            Value = rating.Id.ToString(),
                                            Text = rating.Text
                                       }).ToArray();
            return model;
        }

        [HttpGet("UpdateHappening/{id}")]
        public IActionResult UpdateHappening(UInt64 id)
        {
            try
            {
                IQueryable<Happening> happenings = UnitOfWork.ReadAll<Happening>
                                                   (dbHappening => dbHappening.Id == id);
                if(happenings.Count() == 0)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    Happening happening = happenings.First();
                    CreateUpdateHappeningViewModel model = 
                                              this.GetHappeningViewModel(happening);
                    return View("~/Views/Home/CreateUpdateHappening.cshtml", model);
                }
            }
            catch (Exception e)
            {
                ViewData["Error"] = "Error while returning UpdateHappening view: " + e;
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        private CreateUpdateHappeningViewModel GetHappeningViewModel(Happening happening)
        {
            CreateUpdateHappeningViewModel model = this.GetEmptyHappeningViewModel();
            model.Id = happening.Id;
            model.Name = happening.Name;
            model.RatingId = happening.RatingId;
            model.PreviousPageAction = "GetHappenings";
            return model;
        }

        [HttpPost("CreateUpdateHappening")]
        public IActionResult CreateUpdateHappening(CreateUpdateHappeningViewModel model)
        {
            try
            {
                IActionResult view = ModelState.IsValid ? CreateUpdateHappeningValid(model) :
                                     View("~/Views/Home/CreateUpdateHappening.cshtml", model);
                return view;
            }
            catch (Exception e)
            {
                ViewData["Error"] = "Error while creating or updating happening: " + e;
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        private IActionResult CreateUpdateHappeningValid(CreateUpdateHappeningViewModel model)
        {
            try
            {
                IActionResult view = model.Id == 0 ? CreateHappening(model) : 
                                                     UpdateHappening(model);
                return view;
            }
            catch (Exception e)
            {
                ViewData["Error"] = "Error while creating or updating valid happening: " + e;
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        private IActionResult CreateHappening(CreateUpdateHappeningViewModel model)
        {
            try
            {
                Happening happening = new Happening();
                happening.RegistrationTime = DateTime.UtcNow;
                happening.Name = model.Name;
                happening.RatingId = model.RatingId;
                UnitOfWork.Create<Happening>(happening);
                Int32 page = GetHappeningsMaxPage();
                UpdateState(PageNumberSessionKey, page);
                IActionResult view = this.GetHappenings();
                return view;
            }
            catch (Exception e)
            {
                ViewData["Error"] = "Error while creating happening: " + e;
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        private IActionResult UpdateHappening(CreateUpdateHappeningViewModel model)
        {
            try
            {
                Happening happening = new Happening();
                happening.Id = model.Id;
                happening.Name = model.Name;
                happening.RatingId = model.RatingId;
                UnitOfWork.Update<Happening>(happening);
                IActionResult view = GetHappenings();
                return view;
            }
            catch (Exception e)
            {
                ViewData["Error"] = "Error while updating happening: " + e;
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        [HttpGet("RemoveHappening")]
        public IActionResult RemoveHappening(UInt64 id)
        {
            try
            {
                Happening happening = UnitOfWork.Read<Happening>
                                                (dbHappening => dbHappening.Id == id);
                UnitOfWork.Delete<Happening>(happening);
                IActionResult view = this.GetHappenings();
                return view;
            }
            catch (Exception e)
            {
                ViewData["Error"] = "Error while removing happening: " + e;
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        public IActionResult About()
        {
            try
            {
                ViewData["Info"] = "About";
                ViewData["Message"] = "This page has been created in ASP .NET Core.";
                return View("~/Views/Shared/Info.cshtml");
            }
            catch (Exception e)
            {
                ViewData["Error"] = "Error while showing about page: " + e;
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        public IActionResult Contact()
        {
            try
            {
                ViewData["Info"] = "Contact";
                ViewData["Message"] = "For more information please send an email to: " +
                                      "happenings@example.com";
                return View("~/Views/Shared/Info.cshtml");
            }
            catch (Exception e)
            {
                ViewData["Error"] = "Error while showing contact page: " + e;
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        public IActionResult Documentation()
        {
            try
            {
                ViewData["Info"] = "Documentation";
                ViewData["Message"] = "For more information about ASP .NET Core please " +
                                      "visit: https://en.wikipedia.org/wiki/ASP.NET_Core";
                return View("~/Views/Shared/Info.cshtml");
            }
            catch (Exception e)
            {
                ViewData["Error"] = "Error while showing documentation page: " + e;
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        public IActionResult Licence()
        {
            try
            {
                ViewData["Info"] = "Licence";
                ViewData["Message"] = "This webiste is licensed under GNU GPL 2.0.";
                return View("~/Views/Shared/Info.cshtml");
            }
            catch (Exception e)
            {
                ViewData["Error"] = "Error while showing licence page: " + e;
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        public IActionResult PageNotFound()
        {
            try
            {
                ViewData["Error"] = "Page not found";
                return View("~/Views/Shared/Error.cshtml");
            }
            catch (Exception e)
            {
                ViewData["Error"] = "Error while showing page not found view: " + e;
                return View("~/Views/Shared/Error.cshtml");
            }
        }
    }
}
