using System;

namespace AspExample.ViewModels.Partial
{
    public class PaginationTextBoxViewModel
    {
        public Int32 CurrentPage { get; set; }

        public Int32 MaxPage { get; set; }

        public String Controller { get; set; }

        public String ValueChanged { get; set; }
    }
}

