
using System;
using AspExample.Models.Pocos;
using System.ComponentModel.DataAnnotations;

namespace AspExample.ViewModels
{
    public class GetHappeningsTableViewModel
    {
        public UInt64 Id { get; set; }

        public String Name { get; set; }

        [Display(Name = "Rating")]
        public String RatingName { get; set; }
    }
}