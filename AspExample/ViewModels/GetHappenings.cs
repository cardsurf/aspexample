
using System;
using AspExample.Models.Pocos;
using System.ComponentModel.DataAnnotations;
using AspExample.ViewModels.Partial;
using System.Collections.Generic;

namespace AspExample.ViewModels
{
    public class GetHappeningsViewModel
    {
        public IEnumerable<GetHappeningsTableViewModel> Table { get; set; }

        public PaginationTextBoxViewModel Pagination { get; set; }
    }
}