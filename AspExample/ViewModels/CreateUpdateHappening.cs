
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AspExample.Models.Pocos;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AspExample.ViewModels
{
    public class CreateUpdateHappeningViewModel
    {
        public UInt64 Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Happening name can not be empty")]
        public String Name { get; set; }

        [Required(ErrorMessage = "Rating is required")]
        [Display(Name = "Rating")]
        public UInt64 RatingId { get; set; }

        public SelectListItem[] Ratings { get; set; }
        
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public String PreviousPageAction { get; set; }
    }
}